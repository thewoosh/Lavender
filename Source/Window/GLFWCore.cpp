/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Vulkan and the Vulkan logo are registered trademarks of the Khronos Group
 * Inc.
 *
 * vke is an abbreviation for VulKan Engine.
 */

#include "GLFWCore.hpp"

#include <cassert>
#include <cstdio>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace window {

    [[nodiscard]] inline static constexpr input::KeyboardKey
    translateGLFWKeyToInputKeyboardKey(int key) noexcept {
        switch (key) {
            case GLFW_KEY_W: return input::KeyboardKey::W;
            case GLFW_KEY_A: return input::KeyboardKey::A;
            case GLFW_KEY_S: return input::KeyboardKey::S;
            case GLFW_KEY_D: return input::KeyboardKey::D;
            case GLFW_KEY_LEFT_SHIFT: return input::KeyboardKey::LEFT_SHIFT;
            case GLFW_KEY_LEFT_CONTROL: return input::KeyboardKey::LEFT_CONTROL;
            case GLFW_KEY_SPACE: return input::KeyboardKey::SPACE;
            case GLFW_KEY_KP_0: return input::KeyboardKey::NUMPAD0;
            case GLFW_KEY_KP_1: return input::KeyboardKey::NUMPAD1;
            case GLFW_KEY_KP_2: return input::KeyboardKey::NUMPAD2;
            case GLFW_KEY_KP_3: return input::KeyboardKey::NUMPAD3;
            case GLFW_KEY_KP_4: return input::KeyboardKey::NUMPAD4;
            case GLFW_KEY_KP_5: return input::KeyboardKey::NUMPAD5;
            case GLFW_KEY_KP_6: return input::KeyboardKey::NUMPAD6;
            case GLFW_KEY_KP_7: return input::KeyboardKey::NUMPAD7;
            case GLFW_KEY_KP_8: return input::KeyboardKey::NUMPAD8;
            case GLFW_KEY_KP_9: return input::KeyboardKey::NUMPAD9;
            default: return input::KeyboardKey::INVALID_KEY;
        }
    }

    [[nodiscard]] inline static constexpr input::KeyboardAction
    translateGLFWActionToKeyboardAction(int action) noexcept {
        switch (action) {
        case GLFW_PRESS: return input::KeyboardAction::PRESS;
        case GLFW_RELEASE: return input::KeyboardAction::RELEASE;
        case GLFW_REPEAT: return input::KeyboardAction::REPEAT;
        default:
            assert(false);
            return input::KeyboardAction::INVALID;
        }
    }

    static void
    keyboardCallbackGLFW(GLFWwindow *window, int key, int scanCode, int action, int mods) noexcept {
        static_cast<void>(scanCode);
        static_cast<void>(mods);

        if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE) {
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        }

        const auto inputKey = translateGLFWKeyToInputKeyboardKey(key);
        const auto inputAction = translateGLFWActionToKeyboardAction(action);
        if (inputKey == input::KeyboardKey::INVALID_KEY || inputAction == input::KeyboardAction::INVALID)
            return;

        auto *core = reinterpret_cast<GLFWCore *>(glfwGetWindowUserPointer(window));
        if (core == nullptr || !core->keyboardCallback())
            return;

        core->keyboardCallback()(input::KeyboardUpdate{inputKey, inputAction});
    }

    static void
    mouseButtonCallbackGLFW(GLFWwindow *window, int button, int action, int mods) noexcept {
        auto *core = reinterpret_cast<GLFWCore *>(glfwGetWindowUserPointer(window));
        if (core == nullptr || !core->mouseCallback())
            return;

        static_cast<void>(mods);

        if (core->mouseGrabbed()) {
            if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
                core->setMouseGrabbed(false);
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                return;
            }

            switch (button) {
            case GLFW_MOUSE_BUTTON_LEFT:
                core->mouseCallback()({input::MouseButton::LEFT, action == GLFW_PRESS});
                break;
            case GLFW_MOUSE_BUTTON_MIDDLE:
                core->mouseCallback()({input::MouseButton::MIDDLE, action == GLFW_PRESS});
                break;
            case GLFW_MOUSE_BUTTON_RIGHT:
                core->mouseCallback()({input::MouseButton::RIGHT, action == GLFW_PRESS});
                break;
            default:
                break;
            }
        } else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
            core->setMouseGrabbed(true);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }

    static void
    mousePositionCallbackGLFW(GLFWwindow *window, double posX, double posY) noexcept {
        auto *core = reinterpret_cast<GLFWCore *>(glfwGetWindowUserPointer(window));
        if (core == nullptr || !core->mouseGrabbed() || !core->mouseCallback())
            return;

        int middleX, middleY;
        glfwGetWindowSize(window, &middleX, &middleY);

        middleX /= 2;
        middleY /= 2;

        const double deltaX = middleX - posX;
        const double deltaY = middleY - posY;

        core->mouseCallback()({input::MouseButton::NONE, false, static_cast<float>(deltaX), static_cast<float>(deltaY)});

        glfwSetCursorPos(window, middleX, middleY);
    }

    static void
    resizeCallback(GLFWwindow *window, int width, int height) noexcept {
        assert(width > 0);
        assert(height > 0);

        auto *graphicsAPI = reinterpret_cast<GLFWCore *>(glfwGetWindowUserPointer(window))->graphicsAPI();
        if (graphicsAPI) {
            graphicsAPI->onResize({static_cast<std::uint32_t>(width), static_cast<std::uint32_t>(height)});
        }
    }

    [[nodiscard]] inline constexpr int
    convertGraphicsAPINameToGLFWEnum(GraphicsAPI::Name name) noexcept {
        switch (name) {
            case GraphicsAPI::Name::OPENGL:
                return GLFW_OPENGL_API;
            case GraphicsAPI::Name::VULKAN:
            default:
                return GLFW_NO_API;
        }
    }

    bool
    GLFWCore::initialize(GraphicsAPI::Name graphicsAPI) {
        if (!glfwInit()) {
            std::printf("GLFW: failed to init: %s\n", getGLFWError().data());
            return false;
        }

        glfwWindowHint(GLFW_CLIENT_API, convertGraphicsAPINameToGLFWEnum(graphicsAPI));
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        if (graphicsAPI == GraphicsAPI::Name::OPENGL) {
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        }

        m_window = glfwCreateWindow(1280, 720, "Lavender", nullptr, nullptr);

        if (graphicsAPI == GraphicsAPI::Name::OPENGL) {
            glfwMakeContextCurrent(m_window);
#ifndef NDEBUG
            glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif
        }

        glfwSetWindowUserPointer(m_window, this);

        glfwSetKeyCallback(m_window, reinterpret_cast<GLFWkeyfun>(&keyboardCallbackGLFW));
        glfwSetMouseButtonCallback(m_window, reinterpret_cast<GLFWmousebuttonfun>(&mouseButtonCallbackGLFW));
        glfwSetCursorPosCallback(m_window, reinterpret_cast<GLFWcursorposfun>(&mousePositionCallbackGLFW));
        glfwSetFramebufferSizeCallback(m_window, &resizeCallback);

        return true;
    }

    GLFWCore::~GLFWCore() {
        if (m_window) {
            glfwDestroyWindow(m_window);
        }

        glfwTerminate();
    }

    std::string_view
    GLFWCore::getGLFWError() const noexcept {
        const char *description;
        static_cast<void>(glfwGetError(&description));
        return description;
    }

#ifdef ENABLE_VULKAN
    bool
    GLFWCore::createVulkanSurface(vk::Instance instance,
            const vk::Optional<const vk::AllocationCallbacks> &allocator,
            vk::SurfaceKHR *surfaceDestination) noexcept {
        return glfwCreateWindowSurface(instance, m_window,
                   allocator ? reinterpret_cast<const VkAllocationCallbacks *>(
                                    static_cast<const vk::AllocationCallbacks *>(allocator))
                             : nullptr,
                   reinterpret_cast<VkSurfaceKHR *>(surfaceDestination))
            == VK_SUCCESS;
    }

    base::ArrayView<const char *const>
    GLFWCore::getRequiredVulkanInstanceExtensions() noexcept {
        std::uint32_t size{};

        const char **exts = glfwGetRequiredInstanceExtensions(&size);

        return {exts, size};
    }
#endif

    math::Vector2u
    GLFWCore::queryFramebufferSize() const noexcept {
        int width{}, height{};
        glfwGetFramebufferSize(m_window, &width, &height);

        assert(width > 0);
        assert(height > 0);

        return {static_cast<std::uint32_t>(width), static_cast<std::uint32_t>(height)};
    }

    utils::Version<int>
    GLFWCore::queryGLContextVersion() const noexcept {
        return {
            glfwGetWindowAttrib(m_window, GLFW_CONTEXT_VERSION_MAJOR),
            glfwGetWindowAttrib(m_window, GLFW_CONTEXT_VERSION_MINOR),
            glfwGetWindowAttrib(m_window, GLFW_CONTEXT_REVISION)
        };
    }

    void
    GLFWCore::requestVSyncMode(bool enabled) noexcept {
        if (enabled) {
            glfwSwapInterval(1);
        } else {
            glfwSwapInterval(0);
        }
    }

    bool
    GLFWCore::shouldClose() {
        return glfwWindowShouldClose(m_window) == GLFW_TRUE;
    }

    void
    GLFWCore::preLoop() {
        glfwPollEvents();
    }

    void
    GLFWCore::postLoop() {
        glfwSwapBuffers(m_window);
    }

} // namespace window
